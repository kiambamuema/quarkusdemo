package messaging;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;


@Data
public class EmailRequest {
  @JsonProperty
  public String to;
  @JsonProperty
  public String cc;
  @JsonProperty
  public String sender;
  @JsonProperty
  public String subject;
  @JsonProperty
  public String body;


}
