package messaging;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Builder;

@Builder
@JsonInclude(Include.NON_NULL)
public class EmailResponse {

  @JsonProperty
  public boolean success;
  @JsonProperty
  public String message;
  @JsonProperty
  public EmailRequest emailRequest;
  @JsonProperty
  public List<EmailRequest> emailRequestList;

}
