package com.example.repository;

import javax.inject.Singleton;
import org.eclipse.microprofile.config.inject.ConfigProperty;
@Singleton
public class Config {

  /**
   * postgres config
   */
  @ConfigProperty(name = "postgres_connection_url")
  public String POSTGRES_DB_CON_URL;
  @ConfigProperty(name = "postgres_user")
  public String DB_USER;
  @ConfigProperty(name = "postgres_password")
  public String DB_PASSWORD;
  @ConfigProperty(name = "hikari_min_pool")
  public Integer HIKARI_MIN_POOL;
  @ConfigProperty(name = "hikari_max_pool")
  public Integer HIKARI_MAX_POOL;

  @ConfigProperty(name =  "quarkus.mailer.from")
  public String FROM;


}
