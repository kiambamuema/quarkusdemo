//package GenericApiResponse;
//
//import java.util.List;
//import javax.ws.rs.core.Response;
//
//public interface IRestResponse<T> {
//
//  public T getBody();
//
//  public String getContent();
//
//  public int getStatusCode();
//
//  public boolean isSuccessful();
//
//  public String getDescriptionStatus();
//
//  public Response getResponse();
//
//  public Exception getException();
//
//  public String getReason();
//
//  public String getStatus();
//
//  public List<T> getList();
//
//}
