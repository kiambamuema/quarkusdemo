package com.example.Q;

import java.io.IOException;
import java.util.Scanner;

/**
 * Class NewClass counts total number of words in an input string
 * @author  Nirajkumar Rajendrakumar Patel  (Name of the author)
 * @version 1.0  (Version of the program)
 * @see < a href "~/newclass/com/example/Q/NewClass.html"></>  NewClass documentation file
 */

public class NewClass  {

  /**
   * This is the main class where execution will take place
   * @param args  main class
   * @throws IOException if an input or output exception occurred
   * @throws Exception   in case their is an invalid execution
   */
  public static void main(String[] args) throws Exception,IOException {
    System.out.println("\n\nCount the total number of words in a String:\n");
    System.out.println("--------------------------------------------------");
    System.out.println("Input the string");

    Scanner sc = new Scanner(System.in);
    String input = sc.nextLine();

    System.out.println("Total number of words in the String is: "+ countString(input));
  }

  /**
   *
   * @param word the absolute string to be counted
   * @return int counter (The sum of counted characters)
   * @throws Exception in case of invalid count of string
   * @throws IOException if an input or output exception occurred
   */
  public static int countString(String word) throws Exception, IOException{
    /**
     * initialise counter to zero(0)
     */
    int counter = 0;

    /**
     * loop till the end of the String
     */
    for (int i= 0; i<word.length(); i++){
      /**
       * skip the space
       */
      if (word.charAt(i) != ' '){
        counter++;
      }
    }
    /**
     * return the computed results
     */
    return counter;
  }
}
