package EmailQuartzDemo.controller;

import EmailQuartzDemo.job.EmailJob;
import EmailQuartzDemo.payload.ScheduleEmailRequest;
import EmailQuartzDemo.payload.ScheduleEmailResponse;
//import GenericApiResponse.IRestResponse;
//import GenericApiResponse.RestResponse;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@Path("/emailApp")
public class EmailJobScheduler {
  Logger logger =  LoggerFactory.getLogger(EmailJobScheduler.class);
  @Inject
  Scheduler scheduler;

  @POST
  @Path("/scheduleEmail")
  public Response scheduleEmail(ScheduleEmailRequest request)  {

      ZonedDateTime dateTime = ZonedDateTime.of(request.getDateTime(),request.getTimeZone());
       if (dateTime.isBefore(ZonedDateTime.now())) {
         ScheduleEmailResponse response = ScheduleEmailResponse.builder()
             .success(false)
             .message("dataTime must be after current time")
             .build();
         return Response.ok().status(Status.BAD_REQUEST).entity(response).build();
       }
    try {
      JobDetail jobDetail = buildJobDetail(request);
      Trigger trigger = buildJobTrigger(jobDetail, dateTime);
      CronTrigger cronTrigger = buildJobCronTrigger(jobDetail);
      if (scheduler.checkExists(jobDetail.getKey())){
       logger.info("State for job {} , is  {}",jobDetail.getKey(),
           scheduler.getTriggerState(trigger.getKey()));

//       List<JobExecutionContext> executionContextList = scheduler.getCurrentlyExecutingJobs();
//        System.out.println(executionContextList);
//       for (JobExecutionContext jobExecutionContext : executionContextList) {
//         if (jobExecutionContext.getJobDetail().getKey().getName().equals(jobDetail.getKey().toString()));
//
//         if (scheduler.interrupt(jobExecutionContext.getJobDetail().getKey())) {
//           logger.info("job {} was interrupted ",jobDetail.getKey());
//         }
//       }

//       if (scheduler.unscheduleJob(trigger.getKey())) {
//         logger.info("job {} was unscheduled ",jobDetail.getKey());
//       }


          if (scheduler.interrupt(jobDetail.getKey())) {
            logger.info("job {} was interrupted ",jobDetail.getKey());
          }

        if (scheduler.deleteJob(jobDetail.getKey()) ){
          logger.info("job {} was deleted ",jobDetail.getKey());
        }
      }

      //scheduler.scheduleJob(jobDetail,trigger);
     Date jobTime = scheduler.scheduleJob(jobDetail,trigger);
      logger.info("the job is scheduled to ran at : {} ",jobTime);

      ScheduleEmailResponse response = ScheduleEmailResponse.builder()
          .success(true)
          .message("Email successfully scheduled")
          .jobGroup(jobDetail.getKey().getName())
          .jobId(jobDetail.getKey().getGroup())
          .build();

      return Response.ok().status(Status.OK).entity(response).build();

    }catch (SchedulerException e) {
     logger.error("Error scheduling email {} ", e.getMessage());
     ScheduleEmailResponse response = ScheduleEmailResponse.builder()
         .success(false)
         .message("Error scheduling email. Please try later!")
         .build();
     return Response.ok().status(Status.INTERNAL_SERVER_ERROR).entity(response).build();
    }
  }

  private JobDetail buildJobDetail(ScheduleEmailRequest scheduleEmailRequest) {
    JobDataMap jobDataMap = new JobDataMap();
    jobDataMap.put("email", scheduleEmailRequest.getEmail());
    jobDataMap.put("subject", scheduleEmailRequest.getSubject());
    jobDataMap.put("body", scheduleEmailRequest.getBody());
    jobDataMap.put("orderNo",scheduleEmailRequest.getOrderNo());

    return JobBuilder.newJob(EmailJob.class)
        .withIdentity("scheduledJob" + "_" +scheduleEmailRequest.getOrderNo(), "email-jobs")
        .withDescription("Send Email Job")
        .usingJobData(jobDataMap)
        .storeDurably()
        .build();
  }
  private Trigger buildJobTrigger(JobDetail jobDetail, ZonedDateTime startAt)  {
    return TriggerBuilder.newTrigger()
        .forJob(jobDetail)
        .withIdentity(jobDetail.getKey().getName())
        .withDescription("Send Email Trigger")
        .startAt(Date.from(startAt.toInstant()))
        .withSchedule(SimpleScheduleBuilder.simpleSchedule().withMisfireHandlingInstructionFireNow())
        .build();
  }

  private CronTrigger buildJobCronTrigger(JobDetail jobDetail) {
    return TriggerBuilder.newTrigger()
        .withIdentity(jobDetail.getKey().getName())
        .forJob(jobDetail)
        .withSchedule(CronScheduleBuilder.cronSchedule("0 %d %d %d * ?"))
        .withSchedule(CronScheduleBuilder.atHourAndMinuteOnGivenDaysOfWeek(9,30,5))
        .build();
  }
//  @GET
//  @Blocking
//  public void sentEmail() {
//    mailer.send(Mail.withText("pristonmuema@gmail.com","Test Gmail","I am test mail send"));
//  }
//
//  @GET
//  @Path("/reactive")
//  public CompletionStage<Void> sendMailReactive() {
//    return reactiveMailer.send(Mail.withText("pristonmuema@gmail.com","Test Reactive"
//    ,"I am Testing Reactive")
//    .addAttachment("me","content".getBytes(StandardCharsets.UTF_8),"text/plain")
//    );
//  }



}
