package messaging;

import io.quarkus.vertx.ConsumeEvent;
import javax.enterprise.context.ApplicationScoped;
import javax.json.JsonObject;
import javax.ws.rs.core.Response;
import org.jboss.resteasy.reactive.RestQuery;

@ApplicationScoped
public class GreetingService {

@ConsumeEvent("greeting")
  public EmailRequest greeting(EmailRequest emailRequest){
  return emailRequest;
}
//  @ConsumeEvent("greetings")
//public Response greetings(EmailRequest emailRequest) {
//
//
//  EmailResponse emailResponse = EmailResponse.builder()
//      .success(true)
//      .message("Email successfully sent to " +emailRequest.to)
//      .emailRequest(emailRequest)
//      .build();
//  //return String.valueOf(emailRequest);
//  return Response.ok().entity(emailResponse).build();
//}
}
