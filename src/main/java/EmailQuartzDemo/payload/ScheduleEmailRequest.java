package EmailQuartzDemo.payload;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import javax.validation.constraints.Email;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ScheduleEmailRequest {
  @Email
  @JsonProperty
  public String email;
  @JsonProperty
  public String subject;
  @JsonProperty
  public String body;
  @JsonProperty
  public String orderNo;
  @JsonProperty
  public LocalDateTime dateTime;
  @JsonProperty
  public ZoneId timeZone;
}
