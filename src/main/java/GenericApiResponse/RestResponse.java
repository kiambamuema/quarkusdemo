//package GenericApiResponse;
//
//import io.quarkus.runtime.annotations.RegisterForReflection;
//import java.util.List;
//import javax.enterprise.context.ApplicationScoped;
//import javax.ws.rs.core.Response;
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//@Data
//@NoArgsConstructor
//@AllArgsConstructor
//@RegisterForReflection
//
//public class RestResponse<T> implements IRestResponse {
//
//  private T data;
//  private Response response;
//  private Exception exception;
//  private List<T> listData;
//  private String reason;
//  private String status;
//
////  public RestResponse( Class<T> t, Response response) {
////    this.response = response;
////    try {
////      this.data = t.newInstance();
////    } catch (Exception e) {
////      throw new RuntimeException("There should be a default constructor in the Response POJO");
////    }
////  }
//
//
//  @Override
//  public T getBody() {
//    try {
//      data = (T) response.getEntity();
//    } catch (Exception exception) {
//      this.exception = exception;
//    }
//
//    return data;
//  }
//
//  @Override
//  public String getContent() {
//    return response.getEntity().toString();
//  }
//
//  @Override
//  public int getStatusCode() {
//    return response.getStatus();
//  }
//
//  @Override
//  public boolean isSuccessful() {
//    int code = response.getStatus();
//    if (code == 200 || code == 201 || code == 202 || code == 203 || code == 204 || code == 205)
//      return true;
//    return false;
//  }
//
//  @Override
//  public String getDescriptionStatus() {
//
//    return null;
//  }
//
//  @Override
//  public Response getResponse() {
//    return response;
//  }
//
//  @Override
//  public Exception getException() {
//    return exception;
//  }
//
//  @Override
//  public String getReason() {
//    return reason;
//  }
//
//  @Override
//  public String getStatus() {
//    return status;
//  }
//
//  @Override
//  public List<T> getList() {
//    return listData;
//  }
//
//  public RestResponse<T> success(List<T> list) {
//    return new RestResponse<>(list);
//  }
//}
