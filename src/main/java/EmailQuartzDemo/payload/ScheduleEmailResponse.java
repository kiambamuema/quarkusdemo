package EmailQuartzDemo.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(Include.NON_NULL)
public class ScheduleEmailResponse {
  @JsonProperty
 public boolean success;
  @JsonProperty
 public String jobId;
  @JsonProperty
 public String jobGroup;
  @JsonProperty
 public String message;
}
