package messaging;

import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.core.eventbus.EventBus;
import io.vertx.mutiny.core.eventbus.Message;
import javax.inject.Inject;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@ApplicationPath("Demo")
@Path("async")
@Produces(MediaType.APPLICATION_JSON)
public class EventResource {

  @Inject
  EventBus bus;

  @POST
  public Uni<EmailRequest> greeting(EmailRequest request){
   // return bus.sendAndForget("greeting", request);
    //bus.publish("greeting", request);
    /**
     * send name to the greeting address and request a response
     */
    return bus.<EmailRequest>request("greeting",request)
        /**
         * when we get the response, extract the body and send it to the user
         */

        .onItem().apply(Message::body);

  }

  @POST
  @Path("greetings")
  public Response greetings(EmailRequest request) {
//    EmailResponse response = EmailResponse.builder()
//        .emailRequest()
//        .build();
    bus.<Response>request("greetings",request)
        .onItem().apply(Message::body);
    return Response.ok().build();
  }

}
