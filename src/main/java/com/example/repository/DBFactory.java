package com.example.repository;

import io.agroal.api.AgroalDataSource;
import javax.inject.Inject;
import javax.ws.rs.Produces;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;

public class DBFactory {

  @Inject
  AgroalDataSource dataSource;

  @Produces
  public Jdbi getJdbi() {
    return Jdbi.create(dataSource)
        .installPlugin(new SqlObjectPlugin());
  }
}
