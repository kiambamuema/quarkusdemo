package EmailQuartzDemo.job;

import com.example.repository.Config;
import io.quarkus.mailer.Mail;
import io.quarkus.mailer.Mailer;
import io.smallrye.common.annotation.Blocking;
import java.util.Date;
import javax.inject.Inject;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EmailJob implements Job {


  Logger logger =  LoggerFactory.getLogger(EmailJob.class);
  @Inject
  Mailer mailer;
  @Inject
  Config config;

  private JobKey jobKey = null;
  private volatile boolean isJobInterrupted = false;

  @Override
  public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
   logger.info("Executing Job with key {}",jobExecutionContext.getJobDetail().getKey());


    jobKey = jobExecutionContext.getJobDetail().getKey();
    logger.info("Job {} executing at {} ",jobKey,new Date());
    JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
    String subject = jobDataMap.getString("subject");
    String body = jobDataMap.getString("body");
    String recipientEmail = jobDataMap.getString("email");
    try {
      sendMail(config.FROM,recipientEmail,subject,body);
    }catch (Exception e) {
      logger.error("Error when sending email {}",e.getMessage());
    }finally {
      if (isJobInterrupted) {
        logger.info("Job {} did not complete" ,jobKey);
      }else {
        logger.info("Job {} completed at {} ", jobKey,new Date());
      }
    }


  }
  @Blocking
  public void sendMail(String fromEmail, String toEmail,String subject, String body) {
    try {
      logger.info("Sending Email to {}", toEmail);
      mailer.send(Mail.withText(toEmail,subject,body)
      .setFrom(fromEmail));
      logger.info(" Email sent successfully to {}", toEmail);
    }catch (Exception e) {
      logger.error("Failed to send email to {} {}",toEmail,e.getMessage());
    }
  }


}
